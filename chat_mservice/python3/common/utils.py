# -*- coding: utf-8 -*-
import sys

import requests
import tornado.wsgi
import tornado.httpserver
import tornado.ioloop
import mysql.connector
import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb as mysqlclient
from logging.handlers import RotatingFileHandler
import errno
from global_def import *
from Cryptodome.Cipher import AES
from Cryptodome import Random
from Crypto.Util.Padding import pad
import time
import os
import json
import logging
import subprocess

# region log
# ==============================================================================
def get_logger(name, level=logging.INFO, log_dir=None):
    try:
        # timecr = get_current_datetime()
        # timecr = timecr.strftime(DATETIME_FORMAT3).split("_")[0]
        logger = logging.getLogger(name)
        if log_dir is None:
            log_dir = LOG_DIR_ROOT
        try:
            # log_folder = log_dir + "/" + timecr
            os.makedirs(log_dir)
        except OSError as ex:
            if ex.errno != errno.EEXIST:
                logging.exception(ex)
            # else:
            #     try:
            #         os.makedirs(log_folder)
            #     except OSError as exx:
            #         if exx.errno != errno.EEXIST:
            #             logging.exception(exx)
        except Exception as ex:
            logging.exception(ex)

        # if level is not specified, use level of the root logger
        if level is None:
            level = logging.getLogger().getEffectiveLevel()

        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

        file_h = RotatingFileHandler('{}/{}_{}.log'.format(log_dir, logging.getLevelName(level), name), mode='a',
                                     maxBytes=1024*1024*1024, backupCount=10, encoding=None, delay=0)
        file_h.setFormatter(formatter)

        stdout_h = logging.StreamHandler(sys.stdout)
        stdout_h.setFormatter(formatter)

        if (logger.hasHandlers()):
            logger.handlers.clear()

        logger.addHandler(file_h)
        logger.addHandler(stdout_h)
        logger.setLevel(level)

        return logger
    # ==============================================================================
    # endregion
    except Exception as e:
        logger_db.info("{}".format(e))

logger_db = get_logger("utils_db", level=logging.INFO)

def start_tornado(app, port):
    try:
        max_buffer_size = 1024 * 1024 * 20  # 20M
        http_server = tornado.httpserver.HTTPServer(
            tornado.wsgi.WSGIContainer(app),
            max_buffer_size=max_buffer_size,
        )

        http_server.listen(port)
        tornado.ioloop.IOLoop.instance().start()
    except Exception as e:
        logger_db.info("{}".format(e))

def stop_tornado():
    try:
        tornado.ioloop.IOLoop.instance().stop()
    except Exception as e:
        logger_db.info("{}".format(e))

def is_json(myjson):
    try:
        if isinstance(myjson, dict):
            return True
        json_object = json.loads(myjson)
    except ValueError as e:
        return False
    return True


def make_db_connection(user=MYSQL_DATABASE_USER, password=MYSQL_DATABASE_PASSWORD,
                       host=MYSQL_DATABASE_HOST, database=MYSQL_DATABASE_DB):

    i = 0
    cnx = None
    while True:  # Lap lai ket noi 10 lan trong time 1s.
        try:
            cnx = mysql.connector.connect(user=user, password=password, host=host, database=database)
            return cnx
        except Exception as ex:
            i = i+1
            logger_db.info("Try connect database error number %d " %i)
            pass
        time.sleep(0.1)
        if i >= 10:
            print("Break")
            break
    return cnx

def create_device_token_table(cnx):
    s = "CREATE TABLE device_token ( \
    id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,\
    username VARCHAR(128) DEFAULT \'\' NOT NULL,\
    device_token VARCHAR(256) DEFAULT \'\' NOT NULL);"

    s2 = "SHOW TABLES LIKE \"device_token\";"
    print(s2)
    cursor = cnx.cursor(buffered=True)
    query = s2
    cursor.execute(query)
    res = cursor.fetchall()
    print(res)
    if len(res) == 0:
        print(s)
        query = s
        cursor.execute(query)
        query = s2
        cursor.execute(query)
        res = cursor.fetchall()
        print(res)


def store_device_token_to_db(cnx, kamalio_user, device_token):
    try:
        cursor = cnx.cursor()
        query = "SELECT username FROM device_token WHERE username=\'{}\';".format(
            kamalio_user)
        cursor.execute(query)
        res = cursor.fetchall()
        if len(res) >= 1:  # found
            sql_update_tb = "UPDATE device_token SET device_token=\'{}\' WHERE username=\'{}\'".format(
            device_token, kamalio_user)
            print(sql_update_tb)
            cursor.execute(sql_update_tb)
            cnx.commit()
        else:
            sql_insert_tb = "INSERT INTO device_token (username, device_token) VALUES (%s, %s)"
            values = (kamalio_user, device_token)
            cursor.execute(sql_insert_tb, values)
            cnx.commit()
            print("INSERT username: {},\n device token: {} \nTo device_token table OK".format(kamalio_user, device_token))
    except Exception as ex:
        print("ERROR on insert into device_token: {}".format(ex))


def lookup_device_token_fr_db(kamalio_user, cnx, fname = ""):
    device_token = ""
    try:
        cursor = cnx.cursor(buffered=True)
        query = "SELECT username, device_token, last_update FROM device_token WHERE username=\'{}\';".format(kamalio_user)
        cursor.execute(query)
        res = cursor.fetchall()
        if len(res) >= 1:  # found
            uname = res[0][0]
            device_token = res[0][1]
            last_update = res[0][2]
            return device_token

    except Exception as e:
        print("{}: {}".format(fname, e))

    return device_token



def add_xmpp_user_ejabberd(username, caller = ""):
    data = {
        "user": username,
        "host": KAMAILIO_DOMAIN,
        "password": KAMAILIO_USER_PW
        }
    try:
        r = requests.post(XMPP_HTTP_SERVER + "/api/register", json=data)
        print("SEND to {}/api/registered_users with status {}".format(XMPP_HTTP_SERVER, r.status_code))
        print(r.content)

        if r.status_code == 200:
            return True
    except Exception as ex:
        print("ERROR on creating XMPP user")

    return False

def send_xmpp_message(dest, message, caller = ""):
    data = {
      "type": "chat",
      "from": "pnc.support@wchat.vn",
      "to": dest,
      "subject": "",
      "body": message
    }
    try:
        r = requests.post(XMPP_HTTP_SERVER + "/api/send_message", json=data)
        print("SEND to {}/api/send_message with status {}".format(XMPP_HTTP_SERVER, r.status_code))
        print(r.content)

        if r.status_code == 200:
            return True
    except Exception as ex:
        print("ERROR on creating XMPP user")

    return False



def get_kamailio_user_info(username, cnx, fname=""):
    kamalio_user = None
    try:
        cursor = cnx.cursor(buffered=True)
        query = "SELECT username, domain, password FROM subscriber WHERE username=\'{}\';".format(username)
        cursor.execute(query)
        res = cursor.fetchall()
        if len(res) >= 1: # found
            uname = res[0][0]
            domain = res[0][1]
            password = res[0][2]
            kamalio_user = (uname, domain, password)
    except Exception as e:
        print("{}: {}".format(fname, e))

    return kamalio_user


def create_cc_agent_table(cnx):
    s = "CREATE TABLE cc_agent_info ( \
    id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,\
    username VARCHAR(128) DEFAULT \'\' NOT NULL,\
    email VARCHAR(128) DEFAULT \'\' NOT NULL,\
    team_name VARCHAR(128) DEFAULT \'\' NOT NULL,\
    state INT(2) DEFAULT 0 NOT NULL\
    );"

    s2 = "SHOW TABLES LIKE \"cc_agent_info\";"
    print(s2)
    cursor = cnx.cursor(buffered=True)
    query = s2
    cursor.execute(query)
    res = cursor.fetchall()
    print(res)
    if len(res) == 0:
        print(s)
        query = s
        cursor.execute(query)
        query = s2
        cursor.execute(query)
        res = cursor.fetchall()
        print(res)


def update_cc_agent_state_in_db(cnx, team_name, user_info, state):
    if team_name != "":
        username = user_info.get("username", "")
        email = user_info.get("email", "")
        if username != "":
            try:
                cursor = cnx.cursor()
                query = "SELECT username FROM cc_agent_info WHERE username=\'{}\';".format(
                    username)
                cursor.execute(query)
                res = cursor.fetchall()
                if len(res) >= 1:  # found
                    sql_update_tb = "UPDATE cc_agent_info SET state=\'{}\', team_name=\'{}\' WHERE username=\'{}\'".format(
                        state, team_name, username)
                    print(sql_update_tb)
                    cursor.execute(sql_update_tb)
                    cnx.commit()
                else:
                    sql_insert_tb = "INSERT INTO cc_agent_info (username, email, team_name, state) VALUES (%s, %s, %s, %s)"
                    values = (username, email, team_name, state)
                    cursor.execute(sql_insert_tb, values)
                    cnx.commit()
                    print("INSERT to cc_agent_info table OK")
            except Exception as ex:
                print("ERROR on insert into device_token: {}".format(ex))



def get_cc_agent_state_fr_db(cnx, team_name, fname=""):
    cc_agent_dict = {}
    try:
        cursor = cnx.cursor(buffered=True)
        query = "SELECT username, state FROM cc_agent_info WHERE team_name=\'{}\';".format(team_name)
        cursor.execute(query)
        res = cursor.fetchall()
        for r in res: # found
            username = r[0]
            state = r[1]
            cc_agent_dict[kamailio_user_convert(username)] = state

            # print("user : {}, state {}".format(username, state))
    except Exception as e:
        print("{}: {}".format(fname, e))

    return cc_agent_dict


def kamailio_user_convert(account):
    user = account.lower()
    if "@" in account:
        user = user.replace("@", "_")
    return user