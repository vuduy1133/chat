import flask
from flask import request, jsonify, send_from_directory
import optparse
import json
from global_def import *
from common import utils as ut
import time
import logging
import requests
import os
from threading import Thread
from queue import Queue
import signal
import tornado.ioloop
from flask_cors import CORS


APP_NAME = "extension_manager_service"
logger = ut.get_logger(APP_NAME, level=logging.INFO)

#Config email
app = flask.Flask(APP_NAME)
app.config['SECRET_KEY'] = os.getenv('SECRET_KEY') or os.urandom(16)
CORS(app)

def request_http(url, params=None):
    r = None
    try:
        print(url)
        r = requests.post(url=url, json=params, verify=False)
    except Exception as e:
        print(e)
    return r

default_extension = {
    "username": "anhnt",
    "sip_password": "12345678a@",
    "sip_domain": "wchat.vn",
    "sip_proxy": "wchat.vn:5062",
    "sip_transport": "TCP",
    "expired_time": "-1", #  -1: Inf OR datetime %Y-%m-%d %H:%M:%S
    "sip_uri": "sip:anhnt7@wchat.vn",
    "wss_uri": "wss://wchat.vn:5067"
    }

default_get_extension_api_input = {
    "from": "baopnh@fpt.com.vn",
    "to": "anhnt232@fpt.com.vn", #optional
    "app_name": "MYTINPNC_ANDROID", #required for mobile app, used to lookup for the FCM APP TOKEN
                #  accepted value: MYTINPNC_ANDROID, MYTINPNC_IOS, FC_ANDROID, FC_IOS, HIFPT_ANDROID, HIFPT_IOS
}

default_get_extension_api_output = {
    "from": "baopnh@fpt.com.vn",  #optional
    "from_ext": default_extension,
    "to": "anhnt232@fpt.com.vn",  #optional
    "to_ext": default_extension,  #optional
    "msg": "OK"  #optional
}

default_register_device_token_api_input = {
    "from": "baopnh@fpt.com.vn",
    "device_token": "sdkjflsdjflsdjflkj" # device token used to wakeup app from sleep
}

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Record not found: ' + request.url,
    }
    respone = jsonify(message)
    logger.info('Record not found: ' + request.url)
    respone.status_code = 404
    return respone


@app.route("/api/v1/mobile/get_extension", methods=['POST'])
def mobile_get_extension():
    global g_user_extension_dict
    func_name = "mobile_get_extension"

    try:
        res = request.get_json()
        logger.info("Call {}: {}".format(func_name, res))
        if res is not None and ut.is_json(res):
            fr_extension, to_extension = api_get_extension(res, SIP_SERVER_KAMAILIO, SIP_CLIENT_MOBILE)

            out = make_output(fr_extension, to_extension)
            result = json.dumps(out), 200
            return result

    except Exception as ex:
        logger.exception(ex)
    return not_found()


@app.route("/api/v1/get_extension", methods=['POST'])
def get_extension():
    global g_user_extension_dict
    func_name = "web_get_extension"
    # need to add OAuth here
    try:
        res = request.get_json()
        logger.info("Call {}: {}".format(func_name, res))
        if res is not None and ut.is_json(res):
            fr_extension, to_extension = api_get_extension(res, SIP_SERVER_KAMAILIO, SIP_CLIENT_WEB)

            out = make_output(fr_extension, to_extension)
            result = json.dumps(out), 200
            return result
    except Exception as ex:
        logger.exception(ex)
    return not_found()


@app.route("/api/v1/mobile/register/device_token", methods=['POST'])
def register_device_token():
    global g_user_device_token_dict
    func_name = "register_device_token"
    # need to add OAuth here
    try:
        res = request.get_json()
        result = json.dumps({"msg": "NOT OK"}), 400
        logger.info("Call {}: {}".format(func_name, res))
        if res is not None and ut.is_json(res):
            user = res.get("from", None)
            if user is not None:
                kamailio_user = ut.kamailio_user_convert(user)
                device_token = res.get("device_token", None)
                if device_token is not None:
                    g_user_device_token_dict[kamailio_user] = device_token
                    cnx = ut.make_db_connection(db_user, db_password, db_host, db_name)
                    ut.store_device_token_to_db(cnx, kamailio_user, device_token)
                    cnx.close()
                    result = json.dumps({"msg": "OK"}), 200
        return result
    except Exception as ex:
        logger.exception(ex)
    return not_found()




@app.route("/api/v1/submit", methods=['POST'])
def submit():
    global g_cc_agent_state_dict, g_team_name, g_all_offline, g_not_delivered_queue
    func_name = "submit"
    # need to add OAuth here
    try:
        res = request.get_json()
        result = json.dumps({"msg": "NOT OK"}), 400
        logger.info("Call {}: {}".format(func_name, res))
        if res is not None and ut.is_json(res):
            user = res.get("sender_info", None)
            receiver_extension = res.get("receiver_extension", None)
            msg = res.get("message", None)
            if receiver_extension is not None:
                username = receiver_extension.get("username", "")
                domain = receiver_extension.get("sip_domain", "")
                username = "{}@{}".format(username, domain)

                if username != "":
                    ut.send_xmpp_message(username, msg)
                    logger.info("Send message to {}: {}".format(username, msg))

                result = json.dumps({"msg": "OK"}), 200
        return result
    except Exception as ex:
        logger.exception(ex)
    return not_found()


@app.route("/api/v1/update/cc_agent", methods=['POST'])
def update_cc_agent():
    global g_cc_agent_state_dict, g_team_name, g_all_offline, g_not_delivered_queue
    func_name = "update_cc_agent"
    # need to add OAuth here
    try:
        res = request.get_json()
        result = json.dumps({"msg": "NOT OK"}), 400
        logger.info("Call {}: {}".format(func_name, res))
        if res is not None and ut.is_json(res):
            user = res.get("owner", None)
            cc_agent_info_list = res.get("cc_agent_info_list", None)
            if cc_agent_info_list is not None and len(cc_agent_info_list) >= 1:
                cnx = ut.make_db_connection(db_user, db_password, db_host, db_name)
                for u in cc_agent_info_list:
                    team_name = u.get("team_name", "")
                    state = int(u.get("state", 0))
                    user_info = u.get("user_info", "")
                    if user_info != "":
                        if team_name == g_team_name:
                            username = user_info.get("username", "")
                            if username != "":
                                k_uname = ut.kamailio_user_convert(username)
                                g_cc_agent_state_dict[k_uname] = state
                        ut.update_cc_agent_state_in_db(cnx, team_name, user_info, state)

                g_all_offline = check_cc_agent_all_offline(g_cc_agent_state_dict)
                if not g_all_offline and len(g_not_delivered_queue) > 0:
                    logger.info("Got Queued message, Need to send it now")

                result = json.dumps({"msg": "OK"}), 200
                cnx.close()
        return result
    except Exception as ex:
        logger.exception(ex)
    return not_found()


def check_online_agent():
    global g_stop, g_all_offline, g_not_delivered_queue, g_cc_agent_routing_table_dict
    while not g_stop:
        if g_all_offline:
            logger.info("All offline")
        else:
            if len(g_not_delivered_queue) > 0:
                logger.info("Has Online Agent, get queued message")

        time.sleep(0.1)

    logger.info("While Loop Stopped")

def check_cc_agent_all_offline(cc_agent):
    for d in cc_agent:
        if cc_agent[d] >= 1:
            return False
    return True


def api_get_extension(input_json, sip_server_type = SIP_SERVER_KAMAILIO, sip_client_type = SIP_CLIENT_MOBILE):
    fname = "api_get_extension"
    fr_extension = {}
    to_extension = {}
    cnx = ut.make_db_connection(db_user, db_password, db_host, db_name)
    user = input_json.get("from", None)
    if sip_server_type == SIP_SERVER_KAMAILIO:
        if user is not None:
            kamailio_user = ut.kamailio_user_convert(user)
            if user in g_user_extension_dict:
                fr_extension = g_user_extension_dict[kamailio_user]
            else:
                fr_extension = alloc_extension(kamailio_user, cnx, sip_server_type, sip_client_type)
                g_user_extension_dict[kamailio_user] = fr_extension

        to_tag = input_json.get("to", None)
        if to_tag is not None:
            kamailio_user = ut.kamailio_user_convert(to_tag)
            if to_tag in g_user_extension_dict:
                to_extension = g_user_extension_dict[kamailio_user]
            else:
                to_extension = alloc_extension(kamailio_user, cnx, sip_server_type, sip_client_type)
                g_user_extension_dict[kamailio_user] = to_extension


    cnx.close()

    return fr_extension, to_extension



def make_mobile_extension_info(uname, domain, password):
    default_extension = {
        "username": "{}".format(uname),
        "sip_password": "{}".format(password),
        "sip_domain": "{}".format(domain),
        "sip_proxy": "{}".format(KAMAILIO_TCP_PROXY),
        "sip_transport": "{}".format(KAMAILIO_TCP),
        "expired_time": "-1"  # Inf
    }
    return default_extension


def make_web_extension_info(uname, domain, password):
    default_extension = {
        "username": "{}".format(uname),
        "sip_password": "{}".format(password),
        "sip_domain": "{}".format(domain),
        "expired_time": "-1",  # Inf
        "sip_uri": "sip:{}@{}".format(uname, KAMAILIO_DOMAIN),
        "wss_uri": "wss://{}".format(KAMAILIO_WSS_PROXY)
    }
    return default_extension


def make_output(fr_ext, to_ext):
    default_api_output = {
        "from_ext": fr_ext,
        "to_ext": to_ext,  # optional
        "msg": "OK"  # optional
    }
    return default_api_output


def alloc_extension(kamailio_user, cnx, sip_server_type, sip_client_type):
    global g_user_extension_dict
    fname = "alloc_extension"
    if sip_server_type == SIP_SERVER_KAMAILIO:

        (uname, domain, password) = (kamailio_user, KAMAILIO_DOMAIN, KAMAILIO_USER_PW)

        if ut.add_xmpp_user_ejabberd(kamailio_user, fname):
            logger.info("Ejabberd create user OK")

        if sip_client_type == SIP_CLIENT_MOBILE:
            return make_mobile_extension_info(uname, domain, password)

        if sip_client_type == SIP_CLIENT_WEB:
            return make_web_extension_info(uname, domain, password)

    return None


if __name__ == '__main__':
    try:
        parser = optparse.OptionParser()
        parser.add_option('--port', type='int', default=5001)
        parser.add_option('--db_host', type='string', default='localhost')
        parser.add_option('--db_user', type='string', default='kamailio')
        parser.add_option('--db_name', type='string', default='kamailio')
        parser.add_option('--db_password', type='string', default='kamailiorw')
        parser.add_option('--domain', type='string', default="wchat.vn")
        parser.add_option('--team_name', type='string', default='pnc.support')
        parser.add_option('--xmpp_http_server', type='string', default='http://127.0.0.1:5281')

        opts, args = parser.parse_args()
        port = opts.port
        time_now = get_current_datetime()
        if not os.path.isdir(LOG_DIR_ROOT):
            os.system('mkdir ' + LOG_DIR_ROOT)
        db_user = opts.db_user
        db_host = opts.db_host
        db_password = opts.db_password
        db_name = opts.db_name
        KAMAILIO_DOMAIN = opts.domain

        KAMAILIO_TCP_PROXY = "{}:{}".format(KAMAILIO_DOMAIN, KAMAILIO_TCP_PORT)
        KAMAILIO_WSS_PROXY = "{}:{}".format(KAMAILIO_DOMAIN, KAMAILIO_WSS_PORT)

        XMPP_HTTP_SERVER = opts.xmpp_http_server

        g_team_name = opts.team_name

        g_not_delivered_queue = []

        g_stop = False
        g_cc_agent_task_assigned_dict = {}
        g_cc_agent_routing_table = {} # key: from, value: to

        cnx = ut.make_db_connection(db_user, db_password, db_host, db_name)
        ut.create_cc_agent_table(cnx)
        g_cc_agent_state_dict = ut.get_cc_agent_state_fr_db(cnx, g_team_name)
        g_all_offline = check_cc_agent_all_offline(g_cc_agent_state_dict)
        cnx.close()


        signal.signal(signal.SIGINT, signal.SIG_DFL)

        cnx = ut.make_db_connection(db_user, db_password, db_host, db_name)
        ut.create_device_token_table(cnx)
        cnx.close()

        g_user_extension_dict = {}
        g_user_device_token_dict = {}

        logger.info("Starting Tornado server on port {}".format(port))
        ut.start_tornado(app, port=port)
    except Exception as e:
        logger.info("Main error: {} ".format(e))
