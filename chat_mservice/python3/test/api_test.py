
import requests
import json
from common import utils as ut

def request_http(url, params=None):
    r = None
    try:
        print(url)
        r = requests.post(url=url, json=params, verify=False)
    except Exception as e:
        print(e)
    return r

def oncall_request_test():
    url = "http://{}/{}".format("localhost:5001", "get_extension")

    message = {
        "name":"hifpt-oncall",
        "password": "Admin!@#$%2020"
    }

    r = request_http(url, message)
    print(r.status_code)
    print(json.dumps(r.content))

def kamailio_register_device_token_test(uname, device_token):
    default_register_device_token_api_input = {
        "from": uname,
        "device_token": device_token  # device token used to wakeup app from sleep
    }

    url = kamailio_url_root + "/mobile/register/device_token"

    r = request_http(url, default_register_device_token_api_input)

    print(r.status_code)
    print(json.dumps(r.json(), indent=4))

def kamailio_get_mobile_extension_test( u1 = "anhnt232@fpt.com.vn", u2 = None):
    default_get_extension_api_input = {
        "from": u1,
        "app_name": "MYTINPNC_ANDROID",
    }
    if u2 is not None:
        default_get_extension_api_input["to"] = u2

    url = kamailio_url_root + "/mobile/get_extension"

    r = request_http(url, default_get_extension_api_input)

    print(r.status_code)
    print(json.dumps(r.json(), indent=4))


def kamailio_get_web_extension_test( u1 = "anhnt232@fpt.com.vn", u2 = None):
    default_get_extension_api_input = {
        "from": u1,
    }
    if u2 is not None:
        default_get_extension_api_input["to"] = u2

    url = kamailio_url_root + "/get_extension"

    r = request_http(url, default_get_extension_api_input)

    print(r.status_code)
    print(json.dumps(r.json(), indent=4))


def test_update_cc_agent_info():

    user_info2 = {
        "username": u2,
        "email": u2
    }

    user_info = {
        "username": u1,
        "email": u1
    }

    cc_agent_info = {
        "user_info": user_info,
        "state": 0, # 0 : OFF, 1: ON , 2 BUSY, 3 : DND
        "team_name": "pnc.support" # possible value: pnn.support, tin.support, hifpt.cc
    }

    cc_agent_info2 = {
        "user_info": user_info2,
        "state": 0,  # 0 : OFF, 1: ON , 2 BUSY, 3 : DND
        "team_name": "pnc.support"  # possible value: pnn.support, tin.support, hifpt.cc
    }

    cc_agent_state_update = {
        "owner": user_info, # whose do this action
        'cc_agent_info_list': [
            cc_agent_info, cc_agent_info2
        ]
    }

    url = kamailio_url_root + "/update/cc_agent"


    r = request_http(url, cc_agent_state_update)

    print(r.status_code)
    print(json.dumps(r.json(), indent=4))


def test_submit(username):

    msg = "sdkfjslkdfj"
    receiver_extension = {
        "username": username,
        "sip_domain": "wchat.vn",
    }
    user_info = {
        "username": "system",
        "email": "system"
    }

    cc_agent_state_update = {
        "sender_info": user_info, # whose do this action
        'receiver_extension': receiver_extension,
        "message": msg
    }

    url = kamailio_url_root + "/submit"


    r = request_http(url, cc_agent_state_update)

    print(r.status_code)
    print(json.dumps(r.json(), indent=4))


def test_cc_chat_msg():


    sender_extension = {
        "username": "anhnt7",
        "sip_password": "12345678a@",
        "sip_domain": "wchat.vn",
        "sip_proxy": "wchat.vn:5062",
        "sip_transport": "TCP",
        "expired_time": "-1",  # -1: Inf OR datetime %Y-%m-%d %H:%M:%S
        "sip_uri": "sip:anhnt7@wchat.vn",
        "wss_uri": "wss://wchat.vn:5067"
    }


    sender_info = {
        "username": "anhnt7",
        "email": "anhnt7@fpt.com.vn",
        "fullname": "Nguyen Tuan Anh",
        "nickname": "anhnt7",
        "avatar_img_src": "https://mytinpnc.vn/data/avatar_img/0014594039054011_1.jpg"
    }

    msg_to_web = {
        "title": "SGH00001", #used only for create ticket
        "description": "cần thông tin abc", #used only for create ticket
        "text": "jflgdkg;dkl;gkdgdgpgkfodkdfklkgk;sdkfld",
        "reply_for": "The text that this message reply for",
        "attached_img_src": ["https://mytinpnc.vn/data/tool_support/0073949520716263_1.jpg"],
        "attached_file_src": [],
        "created_date": "06/07/2021 13:52:10",

        "sender_ext": sender_extension,
        "sender_info": sender_info,
    }

    # msg_to_web is sent to the CC account for example: pnc.support
    # The pnc.support then forward to the web agent
    # The web agent reply this message direct to the sender_ext (get from the received message) then change it to
    # his extension


def test_add_kamailio_user():

    d1 = "sdlfkslfk;lsdkfkkkkkkkkkkkkkkkkkkkkkkkkkkk"
    d2 = "sfslkfl;sdkf;klpgpreokkkkkkkkkkkkkkkkkkkkkkkkk"

    kamailio_register_device_token_test(u1, d1)
    kamailio_register_device_token_test(u2, d2)

    kamailio_get_mobile_extension_test(u1)
    kamailio_get_mobile_extension_test(u2)

    kamailio_get_mobile_extension_test(u1, u2)

    kamailio_get_web_extension_test(u1, u2)


def test_ejabberd():
    data = {
        # "user": "anhnt2",
        # "host": "wchat.vn",
        # "password": "12345678a@"
            }
    data = {
        "host":"localhost"
    }
    r = requests.post("http://127.0.0.1:5281/api/registered_users", json=data)
    print(r.status_code)
    print(r.content)


if __name__ == '__main__':
    u1 = "anhnt8"
    u2 = "anhnt232@fpt.com.vn"
    kamailio_url_root = "http://localhost:5001/api/v1"
    #kamailio_url_root = "https://wchat.vn/api/v1"

    # test_update_cc_agent_info()
    test_add_kamailio_user()
    #test_submit("anhnt4")

    # test_ejabberd()
    
    