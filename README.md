# Chạy 3 container sau: ejabberd, mysql, mservice
require:  
        docker engine  
        mysql server  
        
## I. ejabberd 
    Đây là server chat 

    Sửa file ejabberd.yml như sau: 
1. Sửa host với tên miền đang sử dụng, ở ví dụ dưới hãy sửa staging.wchat.vn thành tên miền của anh/chị

![Alt-Text](https://github.com/duyv2/readme/blob/main/host.png?raw=true)  

![Alt-Text](https://github.com/duyv2/readme/blob/main/host_1.png?raw=true)  

![Alt-Text](https://github.com/duyv2/readme/blob/main/host_2.png?raw=true)  

2. Tạo mysql database và bảng để ejabberd kết nối tới  
        script tạo bảng https://github.com/processone/ejabberd/blob/master/sql/mysql.sql

3. Nếu đã có mysql server ở bước 2, trong file ejabberd.yml thì tắt command của dòng code cấu hình kết nối đến mysql và điền thông tin vào từ dòng 33 đến dòng 41 theo thông tin của mysql anh đã cài đặt, dưới đây là ví dụ  
![Alt-Text](https://github.com/duyv2/readme/blob/main/sql_1.png?raw=true)
![Alt-Text](https://github.com/duyv2/readme/blob/main/sql_2.png?raw=true)

4. Từ thư mục chứa file ejabberd.yml, chạy câu lệnh sau  
        docker run -d --name ejabberd -v $(pwd)/ejabberd.yml:/home/ejabberd/conf/ejabberd.yml -p 5222:5222 -p 5443:5443 -p 5281:5281 ejabberd/ecs  

        Diễn giải
        --name: tên container
        -v: đưa file ejabberd.yml vào trong thư mục cấu hình của ejabberd 
        -p: mở port 5222 cho chat app, 5443 cho web, 5281 để mở api

5. Để kiểm tra ejabberd đã chạy chưa, chạy câu lệnh sau  
        docker ps  
        Nếu chạy thành công sẽ hiển thị  
        ![Alt-Text](https://github.com/duyv2/readme/blob/main/dockerpssuccess.png?raw=true)  
        Nếu không thành công sẽ hiển thị  
        ![Alt-Text](https://github.com/duyv2/readme/blob/main/dockerpsfail.png?raw=true)  
        
## II. mysql
Đây là database của microservice (không phải của server chat ejabberd), mục đích của các câu lệnh bên dưới là tạo database với tên là kamailio, tạo user là kamailio với password là kamailiorw. Sau đó gán quyền cho user kamailio có quyền truy cập vào database kamailio

1. Tạo database kamailio  
        CREATE DATABASE kamailio;  
![Alt-Text](https://github.com/duyv2/readme/blob/main/create_db.png?raw=true)


2. Sử dụng database kamailio vừa tạo   
        use kamailio;   
![Alt-Text](https://github.com/duyv2/readme/blob/main/use_db.png?raw=true)


3. Tạo user kamailio  
        CREATE USER 'kamailio'@'localhost';  
![Alt-Text](https://github.com/duyv2/readme/blob/main/create%20user.png?raw=true)


4. Cấp quyền cho user kamailio với password là kamailiorw truy cập vào database kamailio  
        GRANT ALL PRIVILEGES ON kamailio.* To 'kamailio'@'localhost' IDENTIFIED BY 'kamailiorw';  
![Alt-Text](https://github.com/duyv2/readme/blob/main/grant%20privileges.png?raw=true)


5. Kiểm tra xem user kamailio@localhost đã có quyền truy cập vào database kamailio hay chưa  
        SHOW GRANTS for kamailio@localhost;  
![Alt-Text](https://github.com/duyv2/readme/blob/main/show%20grant.png?raw=true)

## III. mservice  
       Đây là microservice quản lí account, source code của file nằm trong thư mục chat_mservice

1. Sửa lại cấu hình như sau  
        Sửa file global_def.py tại đường dẫn chat_mservice\python3\global_def.py  
        Sửa lại KAMAILIO_DOMAIN, đổi wchat.vn với domain đang sử dụng  
![Alt-Text](https://github.com/duyv2/readme/blob/main/mservicefix.png?raw=true)
        
2. cd vào thư mục chat_mservice, từ thư mục này build image với Dockerfile  
        Chạy câu lệnh   
        docker build --tag mservice .  

        Diễn giải  
        Docker tạo ra image từ code với tên là mservice  

        Quá trình chạy 
![Alt-Text](https://github.com/duyv2/readme/blob/main/createimage.png?raw=true)


3. Chạy container từ image vừa tạo  
        Chạy câu lệnh  
        docker run --name mservice -d --net=host mservice  

        Diễn giải  
        --name: Chạy container với tên là mservice  
        -d: chạy dưới background  
        --net=host : sử dụng chế độ network là host  

        Quá trình chạy  
![Alt-Text](https://github.com/duyv2/readme/blob/main/runiamge.png?raw=true)  
        
4. Kiểm tra nếu chạy thành công  
        Chạy câu lệnh  
        docker ps  
![Alt-Text](https://github.com/duyv2/readme/blob/main/psmservice.png?raw=true)  

        Chạy câu lệnh  
        docker logs mservice  
![Alt-Text](https://github.com/duyv2/readme/blob/main/dockerlogsmservice.png?raw=true)    

Nếu cả 2 câu lệnh trên chạy ra kết quả tương tự thì có nghĩa là microservice đã kết nối vào được mysql

